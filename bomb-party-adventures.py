import pyglet
import os
import random
import math
import string

# functions
def splitIntoChars(word):
    #returns a list of characters in the same order as the original string
    return [char for char in word]

def getRandomSubstring(two_letter_chance = 0.25):
    #random word word wordlist
    word = random.choice(WordList.wordlist)
    word_length = len(word)   
    # actually gettting the substring
    if word_length == 2:
        return word
    else:
        # generates substring length so (by default) 25% of the time it's 2 letters and 75% it's 3 letters
        if random.random() <= two_letter_chance:
            ss_length = 2
        else:
            ss_length = 3
        
        location = math.floor(random.uniform(0, word_length - ss_length + 1))
        return word[location:location + ss_length]

def checkWordValidity(word, substring):
    if word in Used.words:
        return 2
    elif (substring in word) and (word in (WordList.wordlist)):
        return 1
    else:
        return 0
    
def getWordsForSubstring(substring, max_words = 5):
    candidate_words = []
    
    for word in WordList.wordlist:
        if substring in word:
            candidate_words.append(word)
    
    if len(candidate_words) <= max_words:
        return sorted(candidate_words)
    else:
        return sorted(random.sample(candidate_words, max_words))

def updateUsedLetters(word):
    letters = splitIntoChars(word)
    for letter in letters:
        Used.letters[letter] = Used.letters[letter] + 1

def checkIfUsedAllLetters(return_list = False):
    unused_letters = []
    if all(value != 0 for value in Used.letters.values()):
        if return_list:
            return unused_letters
        else:
            return True
    else:
        if return_list:
            for key in Used.letters.keys():
                if Used.letters[key] == 0:
                    unused_letters.append(key)
            return unused_letters
        else:
            return False

def resetUsedLetters():
    for char in string.ascii_lowercase:
        Used.letters[char] = 0

def resetGame():
    Player.health = 3
    resetUsedLetters()
    Used.wordCount = 0
    Used.words = []
# classes
class WordList:
    wordlist = []

f1 = open(os.getcwd() + "\\wordlist.txt", "r")
WordList.wordlist = f1.read().splitlines() 
f1.close()

class Used:
    words = []
    letters = {}
    wordCount = 0

resetUsedLetters()

class Player:
    health = 3

# the actual program
difficulty = input('Enter the minimum wpp: ')
play = 'y'
while play == 'y':
    resetGame()
    while not checkIfUsedAllLetters():
        while True:
            substring = getRandomSubstring()
            if len(getWordsForSubstring(substring, max_words = float('inf'))) >= int(difficulty):
                break
        print(substring)
        
        user_word = str(input()).lower()
        
        valid_word_flag = checkWordValidity(user_word, substring)
        if valid_word_flag == 1:
            print('Valid Word!')
            Used.words.append(user_word)
            updateUsedLetters(user_word)
            Used.wordCount += 1
            if checkIfUsedAllLetters():
                print('You used all the letters in ' + str(Used.wordCount) + ' words!')
                #resetUsedLetters()
                for word in Used.words:
                    print(word)
                break
            else:
                print('You still have unused letters! ' + ''.join(checkIfUsedAllLetters(return_list = True)))
        elif valid_word_flag == 2:
            print('You used that word before!')
        else:
            print('Invalid word! Possible words:')
            print(getWordsForSubstring(substring))#, max_words = float('inf')))
            Player.health -= 1
        
        print(Player.health)
        print('')
        
        if not Player.health:
            break
    play = input('Keep playing? (y/n): ').lower()